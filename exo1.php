<?php

$actualYear = 2019;
$age = 30;
//echo "Année ce naissance : " . ($actualYear - $age);

$heights = [160, 175, 180, 145, 190, 181];

// Solution 1 (horrible)
//$sum = $heights[0] + $heights[1] + $heights[2] + $heights[3] + $heights[4];

$heights[3] = 165;

//var_dump($heights);

$sum = array_sum($heights);

$avg = $sum / count($heights);

//echo "La moyenne est : " . round($avg, 2);

//Un tableau peut contenir plusieurs types de variables
$tab = [
    12,
    'Toto',
    [
        12,
        24,
    ],
    true,
    null,
];

$user = [
    'name' => 'Theau',
    'age' => 122,
    'job' => 'Web developper',
];

$message = "Je suis " . $user['name'] . ", j'ai " . $user['age'] . ' ans';
$message .= " et je suis " . $user['job'];

//echo $message;

